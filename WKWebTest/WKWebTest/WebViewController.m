//
//  WebViewController.m
//  WKWebTest
//
//  Created by Melanie Lislie Hsu on 8/21/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController () {
    WKWebView *webView;
}

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    webView = [[WKWebView alloc] init];
    webView.navigationDelegate = self;
    webView.scrollView.bounces = NO;
    [webView.scrollView setBouncesZoom:NO];
    [self addSubView:webView withConstraintWithParent:self.view];
    
    [self writeToFile]; // Test to write a remote URL
    NSData *pdfContent = [self readFromFile];
    [self moveFile];
    
    NSString* location = nil;
    BOOL copyOK = NO;
    NSString* newFolderPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"www"];
    
    // create the folder, if needed
    [[NSFileManager defaultManager] createDirectoryAtPath:newFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
    
    // copy
    NSError* error = nil;
    if ((copyOK = [self copyFrom:self.wwwBundleFolderPath to:newFolderPath error:&error])) {
        location = newFolderPath;
    }
    NSLog(@"Copy from %@ to %@ is ok: %@", NSTemporaryDirectory(), newFolderPath, copyOK? @"YES" : @"NO");
    if (error != nil) {
        NSLog(@"%@", [error localizedDescription]);
        location = nil;
    }
    
    //TODO: can you save something in temp first?
    
    NSString *URL = [self createFileUrlHelper:location];
    NSLog(@"URL: %@", URL);
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URL]]];
}

- (void) writeToFile {
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/Resume.pdf",
                          documentsDirectory];
    //create content - four lines of text
    NSData *content = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"https://github.com/bluemelodia/bluemelodia.github.io/blob/master/index.html"]];
    //save content to the documents directory
    if(content) {
        if ([content writeToFile:fileName atomically:YES]) {
            NSLog(@"YESSSSSS");
        } else {
            NSLog(@"EEK!!!!");
        }
    }
}

- (NSData *) readFromFile {
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/Resume.pdf",
                          documentsDirectory];
    NSData *content = [[NSData alloc] initWithContentsOfFile:fileName];
    return content;
}

-(void) moveFile{
    [[NSFileManager defaultManager] createDirectoryAtPath:@"/tmp/www/" withIntermediateDirectories:YES attributes:nil error:NULL];
    
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *fromPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Resume.pdf"];
    NSString *toPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"www/"];
    
    NSError *copyError = nil;
    if (![[NSFileManager defaultManager] copyItemAtPath:fromPath toPath:toPath error:&copyError]) {
          UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"title" message:[copyError description] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
          [alertView show];
          }
}

- (NSString*) wwwBundleFolderPath
{
    NSBundle* mainBundle = [NSBundle mainBundle];
    NSString* wwwFileBundlePath = [mainBundle pathForResource:@"https://github.com/bluemelodia/bluemelodia.github.io/blob/master/index.html" ofType:@"" inDirectory:@"/tmp/www/"];
    return [wwwFileBundlePath stringByDeletingLastPathComponent];
}

- (BOOL)copyFrom:(NSString*)src to:(NSString*)dest error:(NSError* __autoreleasing*)error
{
    NSFileManager* fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:src]) {
        NSString* errorString = [NSString stringWithFormat:@"%@ file does not exist.", src];
        if (error != NULL) {
            (*error) = [NSError errorWithDomain:@"TestDomainTODO"
                                           code:1
                                       userInfo:[NSDictionary dictionaryWithObject:errorString
                                                                            forKey:NSLocalizedDescriptionKey]];
        }
        return NO;
    }
    
    // generate unique filepath in temp directory
    CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
    CFStringRef uuidString = CFUUIDCreateString(kCFAllocatorDefault, uuidRef);
    NSString* tempBackup = [[NSTemporaryDirectory() stringByAppendingPathComponent:(__bridge NSString*)uuidString] stringByAppendingPathExtension:@"bak"];
    CFRelease(uuidString);
    CFRelease(uuidRef);
    
    BOOL destExists = [fileManager fileExistsAtPath:dest];
    
    // backup the dest
    if (destExists && ![fileManager copyItemAtPath:dest toPath:tempBackup error:error]) {
        return NO;
    }
    
    // remove the dest
    if (destExists && ![fileManager removeItemAtPath:dest error:error]) {
        return NO;
    }
    
    // create path to dest
    if (!destExists && ![fileManager createDirectoryAtPath:[dest stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:error]) {
        return NO;
    }
    
    // copy src to dest
    if ([fileManager copyItemAtPath:src toPath:dest error:error]) {
        // success - cleanup - delete the backup to the dest
        if ([fileManager fileExistsAtPath:tempBackup]) {
            [fileManager removeItemAtPath:tempBackup error:error];
        }
        return YES;
    } else {
        // failure - we restore the temp backup file to dest
        [fileManager copyItemAtPath:tempBackup toPath:dest error:error];
        // cleanup - delete the backup to the dest
        if ([fileManager fileExistsAtPath:tempBackup]) {
            [fileManager removeItemAtPath:tempBackup error:error];
        }
        return NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString*) createFileUrlHelper:(NSString*)folderPath
{
    NSString* path = [folderPath stringByAppendingPathComponent:@"Hsu_Melanie_Resume.pdf"];
    NSURL* url = [NSURL fileURLWithPath:path];
    return url.absoluteString;
}

- (void)addSubView:(UIView*)subview withConstraintWithParent:(UIView*)parent {
    subview.translatesAutoresizingMaskIntoConstraints = NO;
    [parent addSubview:subview];
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                       attribute:NSLayoutAttributeLeft
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:parent
                                                       attribute:NSLayoutAttributeLeft
                                                      multiplier:1.0
                                                        constant:0]];
    
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                       attribute:NSLayoutAttributeRight
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:parent
                                                       attribute:NSLayoutAttributeRight
                                                      multiplier:1.0
                                                        constant:0]];
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                       attribute:NSLayoutAttributeTop
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:parent
                                                       attribute:NSLayoutAttributeTop
                                                      multiplier:1.0
                                                        constant:0]];
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                       attribute:NSLayoutAttributeBottom
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:parent
                                                       attribute:NSLayoutAttributeBottom
                                                      multiplier:1.0
                                                        constant:0]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
